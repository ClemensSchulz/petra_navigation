#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH='/home/clemens/petra_navigation/devel/.private/catkin_tools_prebuild:/opt/ros/melodic:/home/clemens/petra_navigation/devel'
export LD_LIBRARY_PATH='/opt/ros/melodic/lib:/home/clemens/petra_navigation/devel/lib'
export PWD='/home/clemens/petra_navigation/build/catkin_tools_prebuild'
export ROSLISP_PACKAGE_DIRECTORIES='/home/clemens/petra_navigation/devel/.private/catkin_tools_prebuild/share/common-lisp'
export ROS_PACKAGE_PATH='/home/clemens/petra_navigation/build/catkin_tools_prebuild:/opt/ros/melodic/share'